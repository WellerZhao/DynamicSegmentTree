#include "OutputDate.h"

OutputDate::OutputDate()
{
}
OutputDate::OutputDate(int* array, int size)
{
    this->date.clear();
    this->date.reserve(size);
    for (int i = 0; i < size; i++)
    {
        this->date.push_back(*(array + i));
    }
}

OutputDate::~OutputDate()
{
}

void OutputDate::init(int* array, int size)
{
    this->date.clear();
    //this->date.reserve(size);
    for (int i = 0; i < size; i++)
    {
        this->date.push_back(*(array + i));
    }
}

/**********************插入***************************/
void OutputDate::insert(int index, int value)
{
    //index = index > this->date.size() ? this->date.size() : index;  //·ÀÖ¹·ÃÎÊÔ½½ç
    this->date.insert(this->date.begin() + index, value);
}

/**********************更新***************************/
void OutputDate::update(int index, int value)
{
    this->date[index] = value;
}

/**********************更新区间***************************/
void OutputDate::updateInterval(int left, int right, int value)
{
    for (int i = left; i <= right; i++)
    {
        this->date[i] = value;
    }
}

/**********************通过索引查找***************************/
int OutputDate::find(int index)
{
    int count = 0;
    for (int i = 0; i < index; i++)
    {
        count += this->date[i];
    }
    return count;
}

/**********************通过值查找***************************/
int OutputDate::search(int value)
{
    int count = 0;
    int index = 0;
    while (count <= value && index < this->date.size())
    {
        count += this->date[index++];
    }
    if (value >= count)
    {
        return index;
    }
    return index - 1;
}

/**********************隐藏区间***************************/
void OutputDate::hide(int left, int right)
{
    OutputDate::updateInterval(left, right, 0);
}

/**********************删除***************************/
void OutputDate::remove(int index)
{
    if (index > this->date.size())
    {
        index = this->date.size() - 1;
    }
    if(date.empty()){
        cout<<"empty!"<<endl;
        return ;
    }
    this->date.erase(this->date.begin() + index);
}

/**********************输出所有宽度***************************/
void OutputDate::showDate()
{
    cout<<"date\t:";
    for (int i = 0; i < this->date.size(); i++)
    {
        cout<<(i > 0 ? "\t" : "")<<this->date[i];
    }
    cout<<endl;
}

/**********************输出所有索引对应的值***************************/
void OutputDate::showFind()
{
    cout<<"find\t:";
    for (int i = 0; i < this->date.size(); i++)
    {
        cout<<(i > 0 ? "\t" : "")<<find(i);
    }
    cout<<endl<<endl;
}
