#ifndef OUTPUTDATE_H
#define OUTPUTDATE_H

#include <vector>
#include <iostream>

using namespace std;

class OutputDate
{//直接用vector模拟数据结果
    public:
        OutputDate();
        OutputDate(int* array, int size);
        ~OutputDate();

        void init(int* array, int size);                                    //通过数组初始化

        void insert(int index, int value);                                  //在索引为index的位置插入宽度为value的格子
        void update(int index, int value);                                  //更新索引为index的格子宽度为value
        void updateInterval(int left, int right, int value);                //更新索引为[left,right]的格子
        int find(int index);                                                //查询索引为index的格子坐标
        int search(int value);                                              //查询坐标为value的格子索引
        void hide(int left, int right);                                     //隐藏索引为[left,right]的格子
        void remove(int index);                                             //移除索引为index的格子

        void showDate();
        void showFind();

    protected:
    private:
        vector<int> date;                                                   //保存格子宽度
};

#endif // OUTPUTDATE_H
