#ifndef DSTREE_H
#define DSTREE_H

#include <cstddef>

struct Node
{
    int size;                                           //该子树下叶子结点个数
    int width;                                          //该子树下单元格宽度和
    int sign;                                           //标记子树下单元格修改值，-1表示无修改
    int high;                                           //树高
    struct Node* pLeft;                                 //左儿子
    struct Node* pRight;                                //右儿子

    Node():size(1),width(0),sign(-1),high(1),pLeft(NULL),pRight(NULL){};
    Node(int w):size(1),width(w),sign(-1),high(1),pLeft(NULL),pRight(NULL){};

    void pushDownSign();                                //pushdown向下传递标记
    void recalculate();                                 //重新计算结点信息
};

typedef Node* Tree;
typedef Node* TreeNode;

class DSTree
{
    public:
        DSTree();
        DSTree(int *array, int size);
        virtual ~DSTree();

        void buildTree(int *array, int size);                                   //建树
        void destroyTree();                                                     //销毁树

        void insert(int index, int value);                                      //插入结点
        void remove(int index);                                                 //删除结点

        void update(int index, int value);                                      //更新结点
        void updateInterval(int left, int right, int value);                    //更新区间
        void hide(int left, int right);                                         //隐藏区间

        int find(int index);                                                    //通过索引查找值
        int search(int value);                                                  //通过值查找索引

        void showTree();                                                        //显示树信息

    protected:
    private:
        Tree dsTree;

        TreeNode createNode(int width = 0);                                     //创建结点
        TreeNode destoryNode(TreeNode t);                                       //销毁结点

        void f_destroyTree(TreeNode t);                                         //销毁树

        TreeNode f_insert(TreeNode t, int index, int value);                    //插入操作
        TreeNode f_insertTwo(TreeNode t, int index, int value);                 //同时插入两个结点
        TreeNode f_remove(TreeNode t, int index);                               //删除操作

        void f_showTree(TreeNode t, int& num);                                  //先序遍历，显示树信息
        int f_find(TreeNode t, int index, int count);                           //递归查询索引为index的坐标
        int f_search(TreeNode t, int value, int count);                         //递归查询坐标为value的索引

        void f_update(TreeNode t, int index, int value);                        //更新索引为index的单元格宽度为value
        void f_updateInterval(TreeNode t, int left, int right, int value);      //更新索引为区间[left,right]的宽度为value

        TreeNode rotate(TreeNode t);                                            //旋转

        TreeNode leftRotate(TreeNode a, TreeNode b);                            //左旋
        TreeNode rightRotate(TreeNode a, TreeNode b);                           //右旋

        TreeNode left_rightRotate(TreeNode a, TreeNode b, TreeNode c);          //先左旋再右旋
        TreeNode right_leftRotate(TreeNode a, TreeNode b, TreeNode c);          //先右旋再左旋


};

#endif // DSTREE_H
