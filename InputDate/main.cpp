#include <cstdio>
#include <fstream>
#include "InputDate.h"

using namespace std;

void allRandDate();
void allInsertRandDate();
void allFindRandDate();
void allUpdateRandDate();

int main()
{
    //allRandDate();
    //allInsertRandDate();
    //allFindRandDate();
    allUpdateRandDate();

    return 0;
}

void allRandDate()
{
    //输出t组完全随机的数据
    ofstream out("../data/inputDate.in");

    InputDate *input = NULL;
    try
    {
        input = new InputDate(out);
    }catch(bad_alloc &bad)
    {
        clog<<"error: Create InputDate Failed."<<endl;
        return ;
    }

    int t = 100;
    out<<t<<endl;
    while (t--)
    {
        input->showGroupInputDateRand();
    }

    delete(input);
    input = NULL;

    out.close();
}

void allInsertRandDate()
{
    //输出当n、m所有操作为插入操作
    ofstream out("../data/inputDate_all_insert.in");

    InputDate *input = NULL;
    try
    {
        input = new InputDate(out);
    }catch(bad_alloc &bad)
    {
        clog<<"error: Create InputDate Failed."<<endl;
        return ;
    }

    int t = 100;
    out<<t<<endl;

    int x = 10;
    while (t--)
    {
        clog<<x<<endl;
        //input->showGroupInputDateRand();
        input->init(x, x);
        input->showAllInsertRand();
        x += 1000;
    }

    delete(input);
    input = NULL;

    out.close();
}

void allFindRandDate()
{
    //输出所有操作为通过索引查询操作
    ofstream out("../data/inputDate_all_find.in");

    InputDate *input = NULL;
    try
    {
        input = new InputDate(out);
    }catch(bad_alloc &bad)
    {
        clog<<"error: Create InputDate Failed."<<endl;
        return ;
    }

    int t = 100;
    out<<t<<endl;
    int x = 10;
    while (t--)
    {
        clog<<x<<endl;
        input->init(x, x);
        input->showAllFindRand();
        x += 1000;
    }

    delete(input);
    input = NULL;

    out.close();
}

void allUpdateRandDate()
{
    //输出所有操作为更新操作
    ofstream out("../data/inputDate_all_update.in");

    InputDate *input = NULL;
    try
    {
        input = new InputDate(out);
    }catch(bad_alloc &bad)
    {
        clog<<"error: Create InputDate Failed."<<endl;
        return ;
    }

    int t = 100;
    out<<t<<endl;
    int x = 10;
    while (t--)
    {
        clog<<x<<endl;
        input->init(x, x);
        input->showAllupdateRand();
        x += 1000;
    }

    delete(input);
    input = NULL;

    out.close();
}
