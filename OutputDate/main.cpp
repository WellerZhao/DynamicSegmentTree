#include <iostream>
#include <fstream>
#include <string>
#include <ctime>
#include "OutputDate.h"

using namespace std;

const int MAXN = 0xfff00 + 10;
int array[MAXN];

int main()
{/*
    ifstream in("../data/inputDate.in");
    ofstream out("../data/outputDate.out");

    ifstream in("../data/inputDate_all_insert.in");
    ofstream out("../data/outputDate_all_insert.out");

    ifstream in("../data/inputDate_all_find.in");
    ofstream out("../data/outputDate_all_find.out");
*/
    ifstream in("../data/inputDate_all_update.in");
    ofstream out("../data/outputDate_all_update.out");
    int t;
    in>>t;

    OutputDate *output = new OutputDate();

    int test = 1;
    while (t--)
    {
        clock_t start = clock();
        out<<"case "<<test++<<":"<<endl;
        int n;
        in>>n;
        for (int i = 0; i < n; i++)
        {
            in>>array[i];
        }
        output->init(array, n);
        int m;
        in>>m;
        while (m--)
        {
            string str;
            in>>str;
            if (!str.compare("insert")){
                int index, value;
                in>>index>>value;
                output->insert(index, value);
            }else if (!str.compare("update")){
                int index, value;
                in>>index>>value;
                output->update(index, value);
            }else if (!str.compare("updateInterval")){
                int left, right, value;
                in>>left>>right>>value;
                output->updateInterval(left, right, value);
            }else if (!str.compare("find")){
                int index;
                in>>index;
                out<<output->find(index)<<endl;
            }else if (!str.compare("search")){
                int value;
                in>>value;
                out<<output->search(value)<<endl;
            }else if (!str.compare("hide")){
                int left, right;
                in>>left>>right;
                output->hide(left, right);
            }else if (!str.compare("remove")){
                int index;
                in>>index;
                output->remove(index);
            }
        }
        clock_t end = clock();

        clog << (double)(end - start) / CLOCKS_PER_SEC <<endl;
    }
    delete(output);
    output = NULL;

    in.close();
    out.close();
    return 0;
}
