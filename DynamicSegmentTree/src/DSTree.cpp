#include "DSTree.h"
#include "algorithm"
#include "iostream"

/*******************  TreeNode  **********************/
//区间标记向下推
void Node::pushDownSign()
{
    if (Node::sign == -1)
        return ;
    //替换子树的标记
    //维护子树的宽度和
    if (Node::pLeft)
    {
        Node::pLeft->sign = Node::sign;
        Node::pLeft->width = Node::pLeft->size * Node::pLeft->sign;
    }
    if (Node::pRight)
    {
        Node::pRight->sign = Node::sign;
        Node::pRight->width = Node::pRight->size * Node::pRight->sign;
    }
    //修改自己的标记
    Node::sign = -1;
}

//重新计算结点信息
void Node::recalculate()
{
    if (!Node::pLeft)//当前结点为叶子结点
        return ;

    //重新计算结点信息
    Node::size = Node::pLeft->size + Node::pRight->size;
    Node::width = Node::pLeft->width + Node::pRight->width;
    Node::high = std::max(Node::pLeft->high, Node::pRight->high) + 1;
}

/*******************  DSTree  **********************/

DSTree::DSTree()
{
    DSTree::dsTree = NULL;
}

DSTree::DSTree(int* array, int size)
{
    DSTree::dsTree = NULL;
    DSTree::buildTree(array, size);
}

DSTree::~DSTree()
{
    DSTree::destroyTree();
}

//销毁DSTree
void DSTree::destroyTree()
{
    DSTree::f_destroyTree(DSTree::dsTree);
    DSTree::dsTree = NULL;
}

//创建结点
TreeNode DSTree::createNode(int width)
{
    TreeNode node = NULL;
    try
    {
        node = new Node(width);
    }catch(std::bad_alloc &memExp)
    {   //创建结点失败
        std::clog<<"error: Node Create Failed."<<std::endl;
    }
    return node;
}

//销毁结点
TreeNode DSTree::destoryNode(TreeNode t)
{
    if (!t)
    {
        return t;
    }
    delete(t);
    return t = NULL;
}

//插入索引为index的格子宽度为value
void DSTree::insert(int index, int value)
{
    if (!DSTree::dsTree)
    {
        DSTree::dsTree = createNode(value);
        return ;
    }
    if (index > DSTree::dsTree->size)
    {//索引超出范围
        index = DSTree::dsTree->size;
    }
    DSTree::dsTree = DSTree::f_insert(DSTree::dsTree, index, value);
}

//移除索引为index的格子
void DSTree::remove(int index)
{
    if (!DSTree::dsTree)
    {//空树
        std::clog<<"error: DSTree Is Empty."<<std::endl;
        return ;
    }
    if (index >= DSTree::dsTree->size)
    {
        std::clog<<"error: Index Out Of Range."<<std::endl;
        return ;
    }
    if (DSTree::dsTree->size == 1)
    {//根节点是叶子节点
        DSTree::dsTree = DSTree::destoryNode(DSTree::dsTree);
        return ;
    }
    DSTree::dsTree = DSTree::f_remove(DSTree::dsTree, index);
}

//更新索引为index的格子宽度为value
void DSTree::update(int index, int value)
{
    if (index >= DSTree::dsTree->size)
    {
        std::cout<<"error: Index Out Of Range."<<std::endl;
        return ;
    }
    DSTree::f_update(DSTree::dsTree, index, value);
}

//更新区间[left,right]所有格子宽度为value
void DSTree::updateInterval(int left, int right, int value)
{
    if (left > right)
    {
        std::swap(left, right);
    }
    if (left > DSTree::dsTree->size || right > DSTree::dsTree->size)
    {
        std::cout<<"error: Index Out Of Range."<<std::endl;
        return ;
    }
    DSTree::f_updateInterval(DSTree::dsTree, left, right, value);
}

//隐藏区间[left,right]所有格子
void DSTree::hide(int left, int right)
{
    DSTree::updateInterval(left, right, 0);
}

//查询索引为index的格子坐标
int DSTree::find(int index)
{
    //索引超出范围
    index = index > DSTree::dsTree->size ? DSTree::dsTree->size : index;

    return DSTree::f_find(DSTree::dsTree, index, 0);
}

//查询坐标为value的格子索引
int DSTree::search(int value)
{
    //坐标超出范围
    value = value > DSTree::dsTree->width ? DSTree::dsTree->width : value;

    return DSTree::f_search(DSTree::dsTree, value, 0);
}

//先序遍历并输出整棵树的信息
void DSTree::showTree()
{
    int num = 1;
    DSTree::f_showTree(DSTree::dsTree, num);
    std::cout<<std::endl;
}

//递归 遍历整棵树并输出信息
void DSTree::f_showTree(TreeNode t, int& num)
{
    if (!t)
        return ;
    std::cout<<num++<<":\t size:"<<t->size<<"\t width:"<<t->width<<"\t sign:"<<t->sign<<"\t high:"<<t->high<<std::endl;
    DSTree::f_showTree(t->pLeft, num);
    DSTree::f_showTree(t->pRight, num);
}

//建树，循环插入
void DSTree::buildTree(int *array, int size)
{
    DSTree::destroyTree();
    for (int i = 0; i < size; i++)
    {
        DSTree::insert(i, array[i]);
    }
}

//递归销毁DSTree
void DSTree::f_destroyTree(TreeNode t)
{
    if (!t)
        return ;

    DSTree::f_destroyTree(t->pLeft);
    DSTree::f_destroyTree(t->pRight);

    t = DSTree::destoryNode(t);
}

//递归查询索引为index的格子坐标
int DSTree::f_find(TreeNode t, int index, int count)
{
    if (t->size == 1)
    {//叶子结点，递归结束
        return index == 0 ? count : count + t->width;
    }
    if (t->sign != -1)
    {//存在标记，直接计算
        return count + index * t->sign;
    }
    if (index >= t->pLeft->size)
    {
        return DSTree::f_find(t->pRight, index - t->pLeft->size, count + t->pLeft->width);
    }
    else
    {
        return DSTree::f_find(t->pLeft, index, count);
    }
}

//递归查询坐标为value的格子索引
int DSTree::f_search(TreeNode t, int value, int count)
{
    if (t->size == 1)
    {//叶子结点
        return value >= t->width ? count + 1 : count;
    }
    if (t->sign != -1)
    {
        if (value >= t->width)
        {//
            return count + t->size;
        }
        if (t->sign == 0)
        {
            return count + t->size - 1;
        }
        return count + value / t->sign;
    }
    if (value >= t->pLeft->width)
    {
        return DSTree::f_search(t->pRight, value - t->pLeft->width, count + t->pLeft->size);
    }
    else
    {
        return DSTree::f_search(t->pLeft, value, count);
    }
}

//递归更新索引为index的格子宽度为value
void DSTree::f_update(TreeNode t, int index, int value)
{
    if (t->size == 1 && index == 0)
    {
        t->width = t->sign = value;
        return ;
    }

    t->pushDownSign();
    if (t->size == 1)
    {
        t->width = t->sign = value;
        return ;
    }
    if (index >= t->pLeft->size)
    {
        DSTree::f_update(t->pRight, index - t->pLeft->size, value);
    }
    else
    {
        DSTree::f_update(t->pLeft, index, value);
    }
    t->recalculate();
}

//递归更新区间为[left,right]所有格子大小为value
void DSTree::f_updateInterval(TreeNode t, int left, int right, int value)
{
    if (left == 0 && right == t->size - 1)
    {
        t->sign = value;
        t->width = t->sign * t->size;
        return ;
    }

    t->pushDownSign();
    if (left >= t->pLeft->size)
    {
        DSTree::f_updateInterval(t->pRight, left - t->pLeft->size, right - t->pLeft->size, value);
    }
    else if (right < t->pLeft->size)
    {
        DSTree::f_updateInterval(t->pLeft, left, right, value);
    }
    else
    {
        DSTree::f_updateInterval(t->pLeft, left, t->pLeft->size - 1, value);
        DSTree::f_updateInterval(t->pRight, 0, right - t->pLeft->size, value);
    }

    t->recalculate();
}

//插入两个结点
TreeNode DSTree::f_insertTwo(TreeNode t, int index, int value)
{
    //创建要插入的结点
    TreeNode node = DSTree::createNode(value);
    //创建父节点
    TreeNode father = DSTree::createNode();
    if (index == 0)
    {
        father->pLeft = node;
        father->pRight = t;
    }
    else{
        father->pLeft = t;
        father->pRight = node;
    }
    father->recalculate();
    return father;
}

//递归查询插入位置index，插入宽度为value的格子
TreeNode DSTree::f_insert(TreeNode t, int index, int value)
{
    if (t->size == 1)
    {//叶子结点
        return DSTree::f_insertTwo(t, index, value);
    }

    //把查询路径上的标记pushDown
    t->pushDownSign();

    if (t->pLeft->size <= index)
    {//查找右子树
        if (t->pRight->size == 1)
        {//叶子结点，同时插入两个结点
            t->pRight = DSTree::f_insertTwo(t->pRight, index - t->pLeft->size, value);
        }
        else
        {//非叶子结点，继续递归查找
            t->pRight = DSTree::f_insert(t->pRight, index - t->pLeft->size, value);
        }
    }
    else
    {
        if (t->pLeft->size == 1)
        {//叶子结点，同时插入两个结点
            t->pLeft = DSTree::f_insertTwo(t->pLeft, index, value);
        }
        else
        {//非叶子结点，继续递归查找
            t->pLeft = DSTree::f_insert(t->pLeft, index, value);
        }
    }
    //旋转平衡子树
    TreeNode node = DSTree::rotate(t);
    node->recalculate();
    return node;
}

//递归查询索引为index的索引，删除结点
TreeNode DSTree::f_remove(TreeNode t, int index)
{
    t->pushDownSign();

    if (t->pLeft->size <= index)
    {
        if (t->pRight->size == 1)
        {//右子树是叶子结点，删除两个结点
            TreeNode node = t->pLeft;
            t->pRight = DSTree::destoryNode(t->pRight);
            t = DSTree::destoryNode(t);
            return node;
        }
        t->pRight = DSTree::f_remove(t->pRight, index - t->pLeft->size);
    }
    else
    {
        if (t->pLeft->size == 1)
        {//左子树是叶子结点，删除两个结点
            TreeNode node = t->pRight;
            t->pLeft = DSTree::destoryNode(t->pLeft);
            t = DSTree::destoryNode(t);
            return node;
        }
        t->pLeft = DSTree::f_remove(t->pLeft, index);
    }
    //旋转平衡子树
    TreeNode node = DSTree::rotate(t);
    node->recalculate();
    return node;
}

//旋转平衡二叉树
TreeNode DSTree::rotate(TreeNode a)
{
    a->pushDownSign();
    if (a->pLeft->high - a->pRight->high >= 2)
    {
        TreeNode b = a->pLeft;
        b->pushDownSign();
        if (b->pLeft->high >= b->pRight->high)
        {//第一种旋转
            return DSTree::rightRotate(a, b);
        }
        else
        {//第三种旋转
            TreeNode c = b->pRight;
            c->pushDownSign();
            return DSTree::left_rightRotate(a, b, c);
        }
    }
    else if (a->pRight->high - a->pLeft->high >= 2)
    {
        TreeNode b = a->pRight;
        b->pushDownSign();
        if (b->pRight->high >= b->pLeft->high)
        {//第二种旋转
            return DSTree::leftRotate(a, b);
        }
        else
        {//第四种旋转
            TreeNode c = b->pLeft;
            c->pushDownSign();
            return DSTree::right_leftRotate(a, b, c);
        }
    }
    return a;
}

//左旋，第二种平衡
TreeNode DSTree::leftRotate(TreeNode a, TreeNode b)
{
    a->pRight = b->pLeft;
    b->pLeft = a;
    b->recalculate();
    a->recalculate();
    return b;
}

//右旋，第一种平衡
TreeNode DSTree::rightRotate(TreeNode a, TreeNode b)
{
    a->pLeft = b->pRight;
    b->pRight = a;
    b->recalculate();
    a->recalculate();
    return b;
}

//先左旋再右旋，第三种平衡
TreeNode DSTree::left_rightRotate(TreeNode a, TreeNode b, TreeNode c)
{
    a->pLeft = DSTree::leftRotate(b, c);
    return DSTree::rightRotate(a, a->pLeft);
}

//先右旋再左旋，第四种平衡
TreeNode DSTree::right_leftRotate(TreeNode a, TreeNode b, TreeNode c)
{
    a->pRight = DSTree::rightRotate(b, c);
    return DSTree::leftRotate(a, a->pRight);
}

