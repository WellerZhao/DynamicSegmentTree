﻿第一行为T，表示测试数据有t组，后面分别为T组测试数据:
	每组测试数据如下：
	第一行为N，表示初始化格子个数
	第二行为N个整数值，分别表示索引为[0,N-1]的格子宽度Wi
	第三行为M，表示M个操作，有如下操作（每种类型的操作开头都以字符串作为标识：insert，update，find，search，hide，remove）：
		insert index value					//在索引为index的位置插入宽度为value的格子，index为int，value为float
		update index value					//更新索引为index的格子宽度为value，index为int，value为float
		updateInterval left right value		//更新索引为[left,right]的格子，left、right为int，value为float
		find index							//查询索引为index的格子坐标，index为int
		search value						//查询坐标为value的格子索引，value为float
		hide left right						//隐藏索引为[left,right]的格子，left、right为int
		remove index						//移除索引为index的格子，index为int
	后面跟随M行操作：
	