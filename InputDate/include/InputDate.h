#ifndef INPUTDATE_H
#define INPUTDATE_H

#include <iostream>
#include <vector>
#include <fstream>

using namespace std;

class InputDate
{
    public:
        InputDate(ofstream &o);
        InputDate(ofstream &o, int n, int m);                       //初始化数据大小为N
        ~InputDate();

        void init();                                                //重新初始化随机数据
        void init(int n, int m);                                    //重新初始化随机数据

        void showGroupInputDateRand();                              //生成一组随机输入数据

        void showSize();                                            //输出数据大小

        void showAllWidth(int value);                               //输出所有固定大小value的格子
        void showAllWidthRand();                                    //输出随机大小的格子

        void showInsert(int index, int value);                      //输出插入操作
        void showInsertRand();                                      //输出随机插入操作
        void showAllInsertRand();                                   //生成所有随机插入操作

        void showUpdate(int index, int value);                      //输出更新操作
        void showUpdateRand();                                      //输出随机更新操作

        void showUpdateInterval(int left, int right, int value);    //输出更新区间操作
        void showUpdateIntervalRand();                              //输出随机更新区间操作
        void showAllupdateRand();                                   //输出所有随机更新操作

        void showFind(int index);                                   //输出查找索引操作
        void showFindRand();                                        //输出随机查找索引操作
        void showAllFindRand();                                         //输出查询所有索引的操作

        void showSearch(int value);                                 //输出查询坐标操作
        void showSearchRand();                                      //输出随机查询坐标操作

        void showRemove(int index);                                 //输出移除格子操作
        void showRemoveRand();                                      //输出随机移除格子操作

        void showHiden(int left, int right);                        //输出隐藏操作
        void showHidenRand();                                       //输出随机隐藏操作

    protected:
    private:
        const int MAXN              = 0xfff00;                      //格子个数数据范围
        const int MAXN_WIDTH        = 1024;                         //一个格子宽度数据范围
        int N;                                                      //数据大小
        int M;                                                      //操作数量

        vector<int> array;
        ofstream *out;

        void showOperationRand();                                   //随机生成操作

        int sum();                                                  //统计宽度和
};

#endif // INPUTDATE_H
