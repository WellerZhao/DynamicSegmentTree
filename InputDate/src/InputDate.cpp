#include "InputDate.h"
#include <ctime>
#include <cstdlib>

InputDate::InputDate(ofstream &o)
{
    out = &o;
    srand((unsigned) time(NULL));
    InputDate::init();
}
InputDate::InputDate(ofstream &o, int n, int m):N(n), M(m)
{
    out = &o;
    srand((unsigned) time(NULL));
    InputDate::init(n, m);
}

InputDate::~InputDate()
{
}

/*************随机初始化******************/
void InputDate::init()
{
    array.clear();
    InputDate::N = rand() % InputDate::MAXN + 1;
    InputDate::M = rand() % InputDate::MAXN + 1;
}
void InputDate::init(int n, int m)
{
    InputDate::N = n;
    InputDate::M = m;
    array.clear();
}

/*************格子宽度******************/
void InputDate::showAllWidth(int value)
{
    for (int i = 0; i < this->N; i++)
    {
        array.push_back(value);
        (*out)<<(i > 0 ? " " : "")<<value;
    }
    (*out)<<endl;
}
void InputDate::showAllWidthRand()
{
    for (int i = 0; i < this->N; i++)
    {
        int width = rand() % this->MAXN_WIDTH;
        array.push_back(width);
        (*out)<<(i > 0 ? " " : "")<<width;
    }
    (*out)<<endl;
}

/*************插入******************/
void InputDate::showInsert(int index, int value)
{
    array.insert(array.begin() + index, value);
    (*out)<<"insert "<<index<<" "<<value<<endl;
}
void InputDate::showInsertRand()
{
    int index = rand() % (array.size() + 1);
    int width = rand() % MAXN_WIDTH;
    InputDate::showInsert(index, width);
}
void InputDate::showAllInsertRand()
{
    (*out)<<InputDate::N<<endl;
    InputDate::showAllWidthRand();                      //输出N个格子宽度

    (*out)<<InputDate::M<<endl;
    for (int i = 0; i < M; i++)
    {
        InputDate::showInsertRand();
    }
}

/*************更新******************/
void InputDate::showUpdate (int index, int value)
{
    array[index] = value;
    (*out)<<"update "<<index<<" "<<value<<endl;
}
void InputDate::showUpdateRand()
{
    int index = rand() % array.size();
    int width = rand() % MAXN_WIDTH;
    InputDate::showUpdate(index, width);
}

/*************更新区间******************/
void InputDate::showUpdateInterval(int left, int right, int value)
{
    for (int i = left; i <= right; i++)
    {
        array[i] = value;
    }
    (*out)<<"updateInterval "<<left<<" "<<right<<" "<<value<<endl;
}
void InputDate::showUpdateIntervalRand()
{
    int left = rand() % array.size();
    int right = rand() % array.size();
    int width = rand() % MAXN_WIDTH;
    InputDate::showUpdateInterval(min(left, right), max(left, right), width);
}

void InputDate::showAllupdateRand()
{
    (*out)<<InputDate::N<<endl;
    InputDate::showAllWidthRand();                      //输出N个格子宽度

    (*out)<<InputDate::M<<endl;
    for (int i = 0; i < M; i++)
    {
        InputDate::showUpdateIntervalRand();
    }
}

/*************通过索引查找值******************/
void InputDate::showFind(int index)
{
    (*out)<<"find "<<index<<endl;
}
void InputDate::showFindRand()
{
    InputDate::showFind(rand() % array.size());
}
void InputDate::showAllFindRand()
{//输出所有find操作
    (*out)<<InputDate::N<<endl;
    InputDate::showAllWidthRand();                      //输出N个格子宽度

    (*out)<<InputDate::M<<endl;
    for (int i = 0; i < M; i++)
    {
        InputDate::showFindRand();
    }
}

/*************通过值查找索引******************/
void InputDate::showSearch(int value)
{
    (*out)<<"search "<<value<<endl;
}
void InputDate::showSearchRand()
{
    int ss = InputDate::sum();
    //(*out)<<"ss  :"<<ss;
    InputDate::showSearch(rand() % (ss + 1));
}

/*************删除******************/
void InputDate::showRemove(int index)
{
    array.erase(array.begin() + index);
    (*out)<<"remove "<<index<<endl;
}
void InputDate::showRemoveRand()
{
    InputDate::showRemove(rand() % array.size());
}

/*************隐藏******************/
void InputDate::showHiden(int left, int right)
{
    for (int i = left; i <= right; i++)
    {
        array[i] = 0;
    }
    (*out)<<"hide "<<left<<" "<<right<<endl;
}
void InputDate::showHidenRand()
{
    int left = rand() % array.size();
    int right = rand() % array.size();
    InputDate::showHiden(min(left, right), max(left, right));
}

/*************输出一组随机测试数据******************/
void InputDate::showGroupInputDateRand()
{
    InputDate::init();
    (*out)<<InputDate::N<<endl;
    InputDate::showAllWidthRand();                      //输出N个格子宽度

    (*out)<<InputDate::M<<endl;
    for (int i = 0; i < InputDate::M; i++)
    {
        InputDate::showOperationRand();
    }

}

/*************输出随机的操作******************/
void InputDate::showOperationRand()
{
    int ope = rand() % 7;
    //(*out)<<"ope :"<<ope<<"  size :"<<array.size()<<endl;

    switch (ope)
    {
    case 0:
        //insert
        InputDate::showInsertRand();
        break;
    case 1:
        //update
        InputDate::showUpdateRand();
        break;
    case 2:
        //updateInterval
        InputDate::showUpdateIntervalRand();
        break;
    case 3:
        //find:
        InputDate::showFindRand();
        break;
    case 4:
        //search
        InputDate::showSearchRand();
        break;
    case 5:
        //hide
        InputDate::showHidenRand();
        break;
    case 6:
        if (array.size() > 1)
        {
            //remove
            InputDate::showRemoveRand();
        }
        else
        {
            //Insert，保证数组中的大小永远不为0
            InputDate::showInsertRand();
        }
        break;
    };
}

/*************统计所有宽度和******************/
int InputDate::sum()
{
    int count = 0;
    for (int i = 0; i < array.size(); i++)
    {
        count += array[i];
    }
    return count;
}
